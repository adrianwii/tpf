import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {VideoService} from '../../services/video.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  searchString = '';

  @Output()
  searchEvent = new EventEmitter<string>();
  // onKeyDown
  // onSearch

  @Input()
  name: string;

  constructor(private videoService: VideoService) { }

  ngOnInit(): void {
    console.log(this.videoService.getMyVideos())

  }

  sendSearchString(): void {
    console.log(this.searchString);
    this.searchEvent.emit(this.searchString);
  }
}
